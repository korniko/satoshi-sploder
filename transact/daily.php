#!/usr/bin/php -q
<?
	include(__DIR__."/includes/jsonRPCClient.php");
	include(__DIR__."/config.php");

	//Starting balance
	$balance = $rpc->getbalance("pool",0);
	echo "Starting Balance : {$balance}\n";
	if ($balance == 0) {
		echo "Nothing to payout.\n";
		//die();
	} else {
		$rpc->move($poolAccount,$payoutAccount,$balance); //Move to Payout
	}

	//Gather all unsent payouts
	$sends = array();
	$running = 0;
	$query = "SELECT * from pc_trans WHERE sent = 0";
	$res = $db->query($query);
	while($record = $res->fetch()) {
		$tosend = bcdiv( $record["user_fee"] , 100000000, 8 ); //Convert from Satoshi to BTC
		if (isset($sends[$record["trans_account"]])) {
			$sends[$record["trans_account"]] += doubleval($tosend); //Add em up
		} else {
			$sends[$record["trans_account"]] = doubleval($tosend);
		}
		$balance -= doubleval($tosend);
		$running += doubleval($tosend);
		$ids[] = $record["id"];
	}


	echo "Unlocking Wallet.\n";
	$rpc->walletpassphrase($phrase,$timeout);
	echo "Setting TX Fee to {$fee}\n";
	$a = $rpc->settxfee($fee);

	if (count( $sends ) > 0) {
		foreach($sends as $address=>$pay) {
			echo "\t{$address} -> {$pay} \n";
		}
		echo "Sending total of {$running} BTC to ".count($sends)." accounts.\n";

		$a = $rpc->sendmany($payoutAccount,$sends);
		if ($a === false) {
			echo "Error sending.\n";
			echo "Aborted.\n";die();
		}

		echo "Locking Wallet.\n";
		$rpc->walletlock();

		if ( $a ) {
			echo "Completed. Transaction ID : {$a}\n";
			$tn = time();
			$idsj = join(",",$ids);
			echo "Updating Transactions.\n";
			$nquery = "UPDATE pc_trans SET sent={$tn} WHERE id IN ({$idsj})";
			$db->exec($nquery);
		} else {
			echo "Error sending, aborting."; die('Error');
		}
	} else {
		echo "No payouts.\n";
	}

	echo "Resetting Fee.\n";
	$rpc->settxfee(0);
	echo "Ending Balance (take) : {$balance}\n";
?>