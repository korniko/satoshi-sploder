<?php

//*************[ Requires

$f3=require('lib/base.php'); //Fat Free Framework Base
require('includes/bitcoin.php'); //Utility Class
require('config.php'); //Site Configuration
require('includes/jsonRPCClient.php'); //jsonRPC Client
require('includes/sploder.php'); //Sploder Object
require('includes/transact.php'); //Transaction Reader
require('includes/promo.php'); //Controls Promotional information
require('includes/chat.php'); //Chat Wrangler


//*************[ Object Init

$rpc = new jsonRPCClient($f3->get("CONFIG.rpc_url"));
$transact = new transact($rpc);

session_start();
$transact->eatQue();

//*************[ Globals


//*************[ Routes

$f3->route('GET /chat [ajax]', function($f3) {
	$player = $f3->get("COOKIE.player");
	if ($player) { //They are a player
		chat::say( sploder::currentSploder(), $f3->get("GET.m") );
	} else {
		//error_log('nonplayer blah');
	}
});

$f3->route('GET / [ajax]', function($f3) {
	global $transact;
	$f3->set("address",$transact->getAddress());
	$sploder = new sploder( sploder::currentSploder() );
	$sploder->tick();
	echo json_encode($sploder->data());
});
/*
$f3->route('GET /test', function($f3) {
	global $transact;
	$g = $transact->rpc->getinfo();
	var_dump($g);
	die();

});
*/

$f3->route('GET|POST /free/@code [sync]', function($f3) { //Promos
	die();
	global $transact;
	$f3->set("container_class","front");
	$code = $f3->get("PARAMS.code");
	$f3->set("code",$code);
	$f3->set("address","");
	$valid = promo::defs($code);
	if ($valid) {
		if ($_SERVER["REQUEST_METHOD"] == "GET") {
			$f3->set("message","");
		} else {
			if ((! $_SESSION["captcha_code"] ) or (strtolower($_POST["captcha"]) != strtolower($_SESSION["captcha_code"]))) {
				$return = "Invalid Captcha";
			} else {
				$return = promo::check(trim($_POST["address"]));
			}
			$f3->set("address",trim($_POST["address"]));
			if ($return === true) {
				promo::take($_POST["address"]);
				header("Location: /");
				die();
			} else {
				$f3->set("message",$return);
			}
		}
		echo View::instance()->render('header.php');
		if (promo::$left > 0 ) {
			echo View::instance()->render('free.php');
		} else {
			echo View::instance()->render('free-out.php');
		}
		echo View::instance()->render('footer.php');
	} else {
		$f3->error(404);
	}
});
$f3->route('GET /check/@code [sync]', function($f3) { //Promo Check
	$f3->set("container_class","front");
	$code = $f3->get("PARAMS.code");
	$valid = promo::defs($code);
	if ($valid) {
		$f3->set("promoLeft",promo::$left);
		$f3->set("promoAmount",promo::$amount);
		$f3->set("promoNumber",promo::$number);
		$f3->set("promoCode",$code);
		echo View::instance()->render('header.php');
		echo View::instance()->render('check.php');
		echo View::instance()->render('footer.php');
	} else {
		$f3->error(404);
	}
});

$f3->route('GET / [sync]', function($f3) { //BitPass Headpage
	global $transact;
	$f3->set("address",$transact->getAddress());
	$sploder = new sploder( sploder::currentSploder() );
	$f3->set("sploder",$sploder);

	//$f3->set("ticker", json_decode(file_get_contents('http://bcchanger.com/bitcoin_price_feed.php?feed_type=json&currency=USD')));
	$f3->set("container_class","front");
	echo View::instance()->render('header.php');
	echo View::instance()->render('front.php');
	echo View::instance()->render('footer.php');
});

$f3->route('GET /tick', function($f3) {
	$sploder = new sploder( sploder::currentSploder() );
	$sploder->tick();
	//$sploder->fillEmpty();
});

$f3->route('GET /history [ajax]', function($f3) { //History of winnings
	echo sploder::history(true);
});

$f3->route('GET /enum/@amount', function($f3) { //History of winnings
	global $f3;
	echo bitcoin::enum( $f3->get("PARAMS.amount") );
});

$f3->route('GET /captchaImg/@time', function($f3) {
	$img = new Image();
	$img->captcha('fonts/arial.ttf',32,3,'SESSION.captcha_code');
	$img->render();
});

$f3->route('GET /qr', function($f3) { //QR Image Generator
	global $transact;
	include('includes/qr.php');
	$qr = QRencode::factory(QR_ECLEVEL_H,3,4);
	header("Content-Type: image/png");
	$address = $transact->getAddress();
	$qr->encodePNG( $address );
});


//************* Minification

$f3->route('GET /js/@file', function($f3,$args) {
	$f3->set('UI','ui/js/');
	$file = $f3->get("PARAMS.file").".js";
	echo Web::instance()->minify($file);
}, 0 );

$f3->route('GET /css/@file', function($f3,$args) {
	$f3->set('UI','ui/css/');
	$file = $f3->get("PARAMS.file").".css";
	echo Web::instance()->minify($file);
}, 0 );


//*************[ Maintenance Defs

function down() { //Display Maintenance Page
	echo View::instance()->render('down.php');
	die();
}


//*************[ Execute

$f3->run();
