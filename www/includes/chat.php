<?
	class chat {
		static function last_id() {
			global $db,$f3;
			$last_id = $f3->get("SESSION.last_id");
			if ($last_id) {
			} else {
				$query = "SELECT id FROM chat LIMIT 5";
				$res = $db->exec($query);
				if (count($res) > 0) {
					$last_id = $res[0]["id"];
				} else $last_id = 0;
			}
			$query = "SELECT id as seq,player,message FROM chat WHERE id > {$last_id}";
			$res = $db->exec($query);
			if (count($res) > 0) {
				$last = end($res);
				$last_id = $last["seq"];
				$f3->set("SESSION.last_id",$last_id);
				$out = array("c"=>count($res),"m"=>$res);
			} else {
				$out = array("c"=>0);
			}
			return $out;
		}

		static function say($sploder_id,$message) {
			global $f3,$db;
			//Need players number
			$tn = time();

			$session_id = session_id();
			$player = $f3->get("SESSION.player");
			if ($player > 0) { //Currently playing
				//Sanitize message
				$message = htmlspecialchars(trim($message));

				$query = "INSERT INTO chat (
					player,
					sploder_id,
					session_id,
					message,
					created
				) VALUES (
					:player,
					:sploder,
					:session,
					:message,
					:time
				)";
				$db->exec($query, array(
					":player"=>$player,
					":sploder"=>$sploder_id,
					":session"=>$session_id,
					":message"=>$message,
					":time"=>$tn
				));
			} else {
				//error_log('non player trying to chat');
			}
		}
	}
?>