<?php
final class jsonRPCClient {

	/**
	 * Debug state
	 *
	 * @var boolean
	 */
	public $debug=false;

	/**
	 * The server URL
	 *
	 * @var string
	 */
	private $url;
	/**
	 * The request id
	 *
	 * @var integer
	 */
	private $id;
	/**
	 * If true, notifications are performed instead of requests
	 *
	 * @var boolean
	 */
	var $notification = false;

	/**
	 * Takes the connection parameters
	 *
	 * @param string $url
	 * @param boolean $debug
	 */
	public function __construct($url,$debug = false) {
		// server URL
		$this->url = $url;
		// proxy
		empty($proxy) ? $this->proxy = '' : $this->proxy = $proxy;
		// debug state
		empty($debug) ? $this->debug = false : $this->debug = true;
		// message id
		$this->id = 1;
	}

	/**
	 * Sets the notification state of the object. In this state, notifications are performed, instead of requests.
	 *
	 * @param boolean $notification
	 */
	public function setRPCNotification($notification) {
		empty($notification) ?
							$this->notification = false
							:
							$this->notification = true;
	}

	/**
	 * Performs a jsonRCP request and gets the results as an array
	 *
	 * @param string $method
	 * @param array $params
	 * @return array
	 */
	public function __call($method,$params) {

		$debug = "";
		// check
		if (!is_scalar($method)) {
			throw new Exception('Method name has no scalar value');
		}

		// check
		if (is_array($params)) {
			// no keys
			$params = array_values($params);
		} else {
			throw new Exception('Params must be given as array');
		}

		// sets notification or request task
		if ($this->notification) {
			$currentId = NULL;
		} else {
			$currentId = $this->id;
		}

		// prepares the request
		$request = array(
						'method' => $method,
						'params' => $params,
						'id' => $currentId
						);

		$request = json_encode($request);
		$this->debug && $debug.='***** Request *****'."\n".$request."\n".'***** End Of request *****'."\n\n";

		// performs the HTTP POST
		$opts = array ('http' => array (
							'method'  => 'POST',
							'header'  => 'Content-type: application/json',
							'content' => $request
							));

		$context  = stream_context_create($opts);
		$preResponse = '';

		if ($fp = @fopen($this->url, 'r', false, $context)) {
			while($row = fgets($fp)) {
				$preResponse.= trim($row)."\n";
			}
			$this->debug && $debug.='***** Server response *****'."\n".$preResponse.'***** End of server response *****'."\n";
			$response = json_decode($preResponse,true);

		} else {
			if ($this->debug) {
				throw new Exception('Unable to connect to '.$this->url);
			} else {
				error_log("RPC Error while calling method : {$method}");
				//var_dump($preResponse);
				//var_dump($request);
				//die();
				down();
			}
		}

		// debug output
		if ($this->debug) {
			echo nl2br($debug);
		}

		// final checks and return
		if ( ! $this->notification) {
			// check
			if ($response['id'] != $currentId) {
				return false;
				throw new Exception('Incorrect response id (request id: '.$currentId.', response id: '.$response['id'].')');
			}
			if (!is_null($response['error'])) {
				throw new Exception('Request error: '.$response['error']);
			}

			return $response['result'];

		} else {
			return true;
		}
	}
}
?>
