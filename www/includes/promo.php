<?
	class promo {
		static $name;
		static $number;
		static $amount;
		static $left;

		static function defs($name) {
			$promo_a = array(
				"number"=>10,
				"amount"=>50000
			);

			$promos = array(
				"carriage"=>$promo_a,"mile"=>$promo_a,"dirt"=>$promo_a,"wash"=>$promo_a,"friday"=>$promo_a,"gateway"=>$promo_a,"yew"=>$promo_a,"fertilizer"=>$promo_a,"vest"=>$promo_a,"home"=>$promo_a,"asia"=>$promo_a,"cushion"=>$promo_a,"mountain"=>$promo_a,"budget"=>$promo_a,"cucumber"=>$promo_a,"trumpet"=>$promo_a,"lizard"=>$promo_a,"rowboat"=>$promo_a,"spade"=>$promo_a,"hip"=>$promo_a,"boundary"=>$promo_a,"cockroach"=>$promo_a,"weather"=>$promo_a,"yam"=>$promo_a,"octagon"=>$promo_a,"susan"=>$promo_a,"schedule"=>$promo_a,"fly"=>$promo_a,"sousaphone"=>$promo_a,"headline"=>$promo_a,"airport"=>$promo_a,"fur"=>$promo_a,"profit"=>$promo_a,"tabletop"=>$promo_a,"bicycle"=>$promo_a,"witness"=>$promo_a,"crow"=>$promo_a,"room"=>$promo_a,"bomber"=>$promo_a,"eagle"=>$promo_a,"foam"=>$promo_a,"gasoline"=>$promo_a,"plaster"=>$promo_a,"purpose"=>$promo_a,"seagull"=>$promo_a,"step-mother"=>$promo_a,"frame"=>$promo_a,"river"=>$promo_a,"astronomy"=>$promo_a,"corn"=>$promo_a,"road"=>$promo_a,"postage"=>$promo_a,"desire"=>$promo_a,"linda"=>$promo_a,"cocoa"=>$promo_a,"meeting"=>$promo_a,"desert"=>$promo_a,"chick"=>$promo_a,"hallway"=>$promo_a,"stop"=>$promo_a,"seashore"=>$promo_a,"soil"=>$promo_a,"grip"=>$promo_a,"exchange"=>$promo_a,"willow"=>$promo_a,
"spinach"=>$promo_a,"meter"=>$promo_a,"gas"=>$promo_a,"angora"=>$promo_a,"doubt"=>$promo_a,"chicory"=>$promo_a,"palm"=>$promo_a,"clipper"=>$promo_a,"decimal"=>$promo_a,"weeder"=>$promo_a,"crown"=>$promo_a,"softball"=>$promo_a,"close"=>$promo_a,"handle"=>$promo_a,"lobster"=>$promo_a,"criminal"=>$promo_a,"thursday"=>$promo_a,"driving"=>$promo_a,"uncle"=>$promo_a,"discovery"=>$promo_a,"thunder"=>$promo_a,"men"=>$promo_a,"statistic"=>$promo_a,"cork"=>$promo_a,"trial"=>$promo_a,"shrimp"=>$promo_a,"softdrink"=>$promo_a,"december"=>$promo_a,"rake"=>$promo_a,"hydrant"=>$promo_a,"flood"=>$promo_a,"locust"=>$promo_a,"ticket"=>$promo_a,"authority"=>$promo_a,"passive"=>$promo_a);

			if (isset($promos[$name])) {
				self::$name=$name;
				self::$number=$promos[$name]["number"];
				self::$amount=$promos[$name]["amount"];
			} else {
				return false;
			}

			if (self::$number > 0) {
				self::$left = (self::$number) - promo::claimed();
			}

			return true;
		}

		static function claimed() { //Number of promotional units claimed
			global $db;
			$query = "SELECT count(id) as cnt FROM promos WHERE name='".self::$name."'";
			$res = $db->exec($query);
			return $res[0]['cnt'];
		}

		static function check($from_address) {
			global $db;
			$session = session_id();
			$ip = $_SERVER['REMOTE_ADDR'];
			if (bitcoin::checkAddress($from_address)) {
				$query = "SELECT count(id) as cnt FROM promos WHERE (name='".self::$name."' and ip_address='{$ip}') or (name='".self::$name."' and from_address='{$from_address}') or (name='".self::$name."' and session_id='{$session}') LIMIT 1";
				$res = $db->exec($query);
				if ($res[0]['cnt'] > 0) {
					return "You have already claimed";
				}
				return true;
			} else {
				return "Invalid address";
			}
		}


		static function take( $from_address ) {
			global $transact,$db;
			$sploder_id = sploder::currentSploderQue();
			$tn = time();
			$session_id = session_id();
			$ip = $_SERVER["REMOTE_ADDR"];
			$query = "INSERT into promos (
				session_id,
				name,
				from_address,
				ip_address,
				amount,
				created
			) VALUES (
				'{$session_id}',
				'".self::$name."',
				'{$from_address}',
				'{$ip}',
				".self::$amount.",
				{$tn}
			)";
			$db->exec($query);
			$transact->promoAdd( $sploder_id , $from_address, self::$amount);
			return true;
		}

	}
?>
