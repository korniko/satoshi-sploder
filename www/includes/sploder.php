<?
	class sploder {
		var $valid=false;
		var $id;
		var $data;
		var $rpc;

		function __construct( $id=NULL ) {
			global $db;
			if (is_null($id)) return false;
			$query = "SELECT * FROM sploder  WHERE id = {$id} LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) {
				$this->id = $id;
				$this->data = $res[0];
				$this->valid = true;
				return true;
			} else {
				$this->valid = false;
				return false;
			}
		}

		/**
		* currentSploder - returns current sploder for tick
		*
		*/
		static function currentSploder() {
			global $f3,$db;
				$thresh = time() - ($f3->get("CONFIG.sploder_pause"));
				$query = "SELECT id FROM sploder WHERE expires > {$thresh} LIMIT 1";
				$res = $db->exec($query);
				if (count($res) > 0) {
					return $res[0]['id'];
				}
				$query = "SELECT id FROM sploder WHERE expires = 0 LIMIT 1";
				$res = $db->exec($query);
				if (count($res) > 0) {
					return $res[0]['id'];
				} else {
					return sploder::createSploder();
				}
		}

		static function currentSploderQue() { //Current sploder for the que
			global $f3,$db;
			$query = "SELECT id FROM sploder WHERE winner = 0 ORDER by expires DESC LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) {
				return $res[0]['id'];
			}
			$query = "SELECT id FROM sploder WHERE expires = 0";
			$res = $db->exec($query);
			if (count($res) > 0) {
				return $res[0]['id'];
			}
			return sploder::createSploder();
		}

		static function createSploder() {
			global $f3,$db;
			$tn = time();
			$query = "INSERT INTO sploder (
				created
			) VALUES (
				{$tn}
			)";
			$res = $db->query($query);
			return $db->lastInsertId();
		}

		static function history( $html=true) {
			global $f3,$db;
			$query = "SELECT * FROM sploder WHERE winner > 0 ORDER BY expires DESC LIMIT 8";
			$res = $db->query($query);
			if ($html == true) {
				$buffer = "";
				foreach($res as $k=>$v) {
					$w = time() - $v["expires"];
					$buffer .= "<TR><TD>".self::_ago( $v["expires"] )."</TD><TD>".bitcoin::enum( $v['pool'] )."</TD></TR>\n";
				}
				return $buffer;
			} else {
				return $res;
			}
		}

		static function _ago($ptime) {
			$etime = time() - $ptime;
			if ($etime < 1) {
				return '0 seconds';
			}

			$a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
						30 * 24 * 60 * 60       =>  'month',
						24 * 60 * 60            =>  'day',
						60 * 60                 =>  'hour',
						60                      =>  'minute',
						1                       =>  'second'
			);

			foreach ($a as $secs => $str) {
				$d = $etime / $secs;
				if ($d >= 1) {
					$r = round($d);
					return $r . ' ' . $str . ($r > 1 ? 's' : '');
				}
			}
		}

		function fillEmpty() { //Find empty sploder and fill it with a bet
			global $f3,$db,$transact;
			$query = "SELECT * FROM sploder WHERE expires = 0 and winner = 0 LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) {
				if ((time() - $res[0]["created"]) > $f3->get("CONFIG.sploder_stale")) {
					$transact->fill();
				}
			}
		}

		function tick() { //Time sensitive functions
			global $f3,$db,$transact;
			if ((($this->data["expires"] == 0) and ($this->data["players"] > 1) and ($this->data["pool"] >= $f3->get("CONFIG.sploder_min")))) { //Start Timer
				$expires = (time() + $f3->get("CONFIG.sploder_life"));
				$query = "UPDATE sploder SET expires = ".$expires." WHERE id = {$this->id}";
				$db->exec($query);
				$this->data["expires"] = $expires;
			}

			if ($this->data["winner"] == 0) {
				if (($this->data["expires"] > 0) and (time() > $this->data["expires"])) { //Sploder Sploded
					$winner = NULL;
					$max = 0;
					$winner_found = false;
					$rcount = 0;
					$total = 0;
					$k = 0;
					$spin = array();
					$query = "SELECT from_address, min(created) as mc, SUM(amount) AS total FROM sploder_pool WHERE sploder_id = {$this->id} GROUP BY from_address ORDER BY mc";
					$res = $db->exec($query);
					foreach($res as $slot=>$v) {
						$spin[$slot] = $v['total'];
						$map[$slot] = $v['from_address'];
						$total = $total + $v['total'];
					}
					foreach($spin as $k=>$v) $max += $v;
					$roll = mt_rand(0,$max);
					error_log("roll : {$roll}/{$max}");
					foreach($spin as $k=>$v) {
						$rcount = $rcount + $v;
						if (( $roll <= $rcount ) and ( ! $winner_found)) {
							$winner = ($k+1);
							$winner_found = true;
						}
					}
					if ( ! $winner_found ) { //Shoud never happen, but just in case
						$winner = 1;
					}

					$query = "UPDATE sploder SET winner = {$winner} WHERE id = {$this->id}"; //Closes current sploder
					$db->exec($query);
					$this->data["winner"] = $winner;

					//Transfer Winnings
					$winnings = ($total - $spin[$winner-1]);
					$fee = bcmul($winnings ,  $f3->get("CONFIG.service_fee"), 8);
					$won = ($winnings - $fee) + $spin[$winner-1];
					if ($transact->winner($map[$winner-1],$won)) {
						$tn = time();
						$query = "UPDATE sploder SET paid = {$tn} WHERE id = {$this->id}"; //Make sure it was paid out
						$db->exec($query);
					}

				}
			}
		}

		function data() {
			global $f3,$db,$transact;
			$out = array();
			$data = array();
			$fullTotal = 0;
			$players = 0;
			$slot = 0; $total=0;
			if ($this->data["winner"] > 0) $out["wc"] = $f3->get("CONFIG.colors.lose");
			$query = "SELECT from_address, min(created) as mc, SUM(amount) AS total FROM sploder_pool WHERE sploder_id = {$this->id} GROUP BY from_address ORDER BY mc";
			$res = $db->exec($query);
			$player = 0;
			$players = 0;
			if (count($res) == 0) { //Empty Sploder
				$data = array(array("c"=>$f3->get("CONFIG.colors.empty"),"v"=>1));
				$out["h"] = $f3->get("CONFIG.sploder_empty");
				$out["t"] = "0";
			} else {
				$slot=0;
				foreach($res as $v) {
					$players++;
					$fullTotal += $v['total'];
					if (( ! is_null($v['from_address'])) and ($v['from_address'] == $transact->from_address)) {	//Current User
						$color = $f3->get("CONFIG.colors.self");
						$total += $v['total'];
						if (($this->data["winner"] - 1) == $slot) {
							$color = $f3->get("CONFIG.colors.win");
							$isWin=1;
						}
						$player = $slot+1;
					} else { //Someone Else
						$others = $f3->get("CONFIG.colors.others");
						$color_index = ($slot % count($others));
						$color = $others[$color_index];
						if (($this->data["winner"] - 1) == $slot) {
							$color = $f3->get("CONFIG.colors.lose");
							$isWin=2;
						}
						$total += bcmul( $v['total'] , (1 - $f3->get("CONFIG.service_fee")) ,8);
					}


					if (isset($isWin)) {
						$data[] = array("c"=>$color,"v"=>$v['total'],"vh"=>bitcoin::enum($v['total']),"i"=>$isWin);
						unset($isWin);
					} else {
						$data[] = array("c"=>$color,"v"=>$v['total'],"vh"=>bitcoin::enum($v['total']));
					}
					$slot++;
				}
				$f3->set("COOKIE.players",$players);

				$html_total = "<div id=\"pot\">".bitcoin::enum($fullTotal)."</div>";

				if ($players == 1) {
					$out["h"] = "{$html_total} <BR>Waiting for other players";
				} elseif ($fullTotal < $f3->get("CONFIG.sploder_min")) {
					$out["h"] = "{$html_total} <BR>Waiting for larger pool<BR>(".bcdiv($f3->get("CONFIG.sploder_min"),bitcoin::SATOSHI,8)." minimum)";
				} else {
					$out["h"] = $players." Players <BR><BR><BR>"."{$html_total}";
				}

				$out["t"] = $fullTotal;
				$out["p"] = $player;
				$f3->set("COOKIE.player",$player);
				$out["c"] = chat::last_id();

			}
			for($i=0; $i < $f3->get("CONFIG.new_players_per_refresh"); $i++) {
				$data[] = array("v"=>'');
			}
			$out['d'] = $data;
			if ($this->data["winner"]) {
				$out["w"] = $this->data["winner"];
			}

			if ($this->data["expires"] > 0) {
					$out["e"] = ($this->data["expires"] - time());
			} else {
					$out["e"] = 0;
			}

			return $out;
		}
	}
?>