<?
	class transact {
		var $valid;
		var $rpc;
		var $from_address;
		var $top_txid;

		function __construct($rpc=NULL) {
			if (is_null($rpc)) return false;
			$this->rpc = $rpc;
			$this->valid = true;
		}

		function getAddress() {
			global $f3,$db;
			//Check if user currently has address
			if ($f3->get("COOKIE.session_id")) {
				$session_id = $f3->get("COOKIE.session_id");
			} else {
				$session_id = session_id();
				$f3->set("COOKIE.session_id",$session_id);
			}
			$th = (time() - $f3->get("CONFIG.threshold_locked"));

			$query = "SELECT id,address,from_address FROM address_pool WHERE locked >= {$th} AND session_id ='{$session_id}' LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) { //Already has one
				$this->from_address = $res[0]["from_address"];
				$address = $res[0]["address"];
				return $address;
			}

			$query = "SELECT id,address FROM address_pool WHERE locked < {$th} LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) { //Grabbed a spare
				$id = $res[0]['id'];
				$tn = time();
				$query = "UPDATE address_pool SET locked = {$tn}, session_id='{$session_id}', from_address='' WHERE id = {$id}";
				$db->query($query);
				$this->from_address = '';
				return $res[0]['address'];
			} else { //Create One
				if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
				if ($f3->get("CONFIG.wallet")) $this->rpc->walletpassphrase($f3->get("CONFIG.wallet"),2);
				//$this->rpc->keypoolrefill();
				$address = $this->rpc->getnewaddress($f3->get("CONFIG.account_pool"));
				if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
				$this->from_address = '';
				$created = time();
				$db->exec("INSERT into address_pool (address,session_id,locked,created) VALUES ('{$address}','{$session_id}',{$created},{$created})");
				return $address;
			}
		}


		function eatQue( $sploder_id = NULL ) {
			global $f3,$db;
			$record = false;
			if ( is_null($sploder_id) ) {
				$sploder_id = sploder::currentSploderQue();
			}

			$query = "SELECT expires FROM sploder WHERE id = {$sploder_id}";
			$res = $db->exec($query);
			if (count($res) == 0) return false;
			$sploder_expire = $res[0]['expires'];
			if ($sploder_expire == 0) {
				$sploder_expire = (time() + 3600);
			}

			$query = "SELECT txid FROM sploder_pool WHERE isPromo IS NULL ORDER BY id DESC LIMIT 1";
			$res = $db->exec($query);
			if (count($res) > 0) {
				$last_txid = $res[0]['txid'];
			} else {
				$last_txid = NULL;
				$record = true;
			}


			$transactions =  $this->rpc->listtransactions($f3->get("CONFIG.account_pool"),$f3->get("CONFIG.maxTrans")) ;
			//var_dump($transactions);die();

			foreach($transactions as $tran) {
				if ($record) { //This is a new record
					if ($tran["category"] == "receive") {
						$created = $tran["timereceived"];
						$to_find = $tran["address"];
						$session_id = '';
						$to_address = '';
						$from_address = '';
						$amount = 0;
						$txid = $tran["txid"];
						$to_address = $tran["address"];
						$dat = $this->rpc->getrawtransaction($txid, 1);
						$pubKey = explode(" ",$dat["vin"][0]["scriptSig"]["asm"]);
						$from_address = bitcoin::pubKeyToAddress($pubKey[1]);

						$squery = "SELECT id,session_id,from_address FROM address_pool WHERE address = '{$to_address}' LIMIT 1";
						$sres = $db->exec($squery);
						if (count($sres) > 0) { //User Session Exists
							$srecord = $sres[0];
							$session_id = $srecord['session_id'];
							if ($srecord['from_address'] == '') {
								$tn = time();
								$nquery = "UPDATE address_pool SET locked={$tn}, from_address = '{$from_address}' WHERE id = {$srecord['id']}";
								$db->exec($nquery);
							}
						}
						$amount = $tran["amount"] * bitcoin::SATOSHI;

						$tn = time();
						//Check for unique tx first
						$query = "SELECT id FROM sploder_pool WHERE txid = '{$txid}' LIMIT 1";
						$res = $db->exec($query);
						if (count($res) == 0) {

							$query = "SELECT COUNT(DISTINCT(from_address)) as players FROM sploder_pool WHERE sploder_id = {$sploder_id}";
							$res = $db->exec($query);
							$pre_players = $res[0]['players'];

							$query = "INSERT into sploder_pool (
								sploder_id,
								amount,
								session_id,
								txid,
								created,
								from_address
							) VALUES (
								{$sploder_id},
								{$amount},
								'{$session_id}',
								'{$txid}',
								{$tn},
								'{$from_address}'
							)";
							$db->exec($query);

							//Update Sploder Total Players
							$query = "SELECT COUNT(DISTINCT(from_address)) as players FROM sploder_pool WHERE sploder_id = {$sploder_id}";
							$res = $db->exec($query);
							$post_players = $res[0]['players'];

							if ($post_players > 2) { //Bump Time
								$player_change = ($post_players - $pre_players);
								$add_time = ($player_change * $f3->get("CONFIG.bump_time"));
								if ($add_time > 0) {
									$new_expires = ($sploder_expire + $add_time);
									$query = "UPDATE sploder set expires = {$new_expires} WHERE id = {$sploder_id} AND expires > 0 AND winner = 0";
									$db->exec($query);
								}
							}

							$query = "UPDATE sploder SET players = {$post_players} WHERE id = {$sploder_id}";
							$db->exec($query);

							$this->updateSum($sploder_id);
						}
					}
				}

				//var_dump($tran["txid"]);die();
				//var_dump($last_txid);die();

				if (isset($tran["txid"]) and ($tran["txid"] == $last_txid) and ($tran['timereceived'] < $sploder_expire)) $record = true; //Where we left off
			}
		}

		function promoAdd( $sploder_id , $from_address , $amount=50000 ) { //Used for freebie low amount trans
			global $db;
			$tn = time();
			$query = "SELECT COUNT(DISTINCT(from_address)) as players FROM sploder_pool WHERE sploder_id = {$sploder_id}";
			$res = $db->exec($query);
			$pre_players = $res[0]['players'];
			if ($f3->get("COOKIE.session_id")) {
				$session_id = $f3->get("COOKIE.session_id");
			} else {
				$session_id = session_id();
				$f3->set("COOKIE.session_id",$session_id);
			}
			$txid = $this->gentxid(); //random

			$query = "INSERT into sploder_pool (
				sploder_id,
				amount,
				session_id,
				txid,
				created,
				from_address,
				isPromo
			) VALUES (
				{$sploder_id},
				{$amount},
				'{$session_id}',
				'{$txid}',
				{$tn},
				'{$from_address}',
				1
			)";
			$db->exec($query);

			$this->updateSum($sploder_id);

			$query = "UPDATE sploder SET players = {$pre_players} WHERE id = {$sploder_id}";
			$db->exec($query);
		}

		function gentxid() {
			$string = "";
			for( $i=0 ; $i < 40 ; $i++ ) {
				$buffer = dechex(rand(0,255));
				if (strlen($buffer) == 1) {
					$string .= "0".$buffer;
				} else {
					$string .= $buffer;
				}
			}
			return $string;
		}

		function updateSum($sploder_id) {
			global $db;
			//Update Pool Sum
			$query = "SELECT SUM(amount) as total FROM sploder_pool WHERE sploder_id = {$sploder_id}";
			$res = $db->exec($query);
			$query = "UPDATE sploder SET pool = {$res[0]['total']} WHERE id = {$sploder_id}";
			$db->exec($query);
		}

		function fill($amount=500000) {
			global $f3;
			$btcAmount = bcdiv($amount, bitcoin::SATOSHI, 8 );
			$this->rpc->settxfee(0);
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletpassphrase($f3->get("CONFIG.wallet"),2);
			$this->rpc->sendfrom( $f3->get("CONFIG.account_payout") , $f3->get("CONFIG.sploder_stale_to"), (float)$btcAmount, 0);
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
			$this->rpc->settxfee(0);
			error_log("Self-filling Sploder");
		}

		function winner($from_address,$amount) {
			global $f3;
			$btcAmount = bcdiv($amount, bitcoin::SATOSHI, 8 );
/*
			$balance = $this->rpc->getbalance($f3->get("CONFIG.account_pool"),0);
			$new_balance = bcsub($balance,$f3->get("CONFIG.trans_fee"),8);
			if ($new_balance > 0) {
				error_log("Moving {$new_balance} from ".$f3->get("CONFIG.account_pool")." to ".$f3->get("CONFIG.account_payout")." Paying to '{$from_address}' amount: ". doubleval($btcAmount));
				$this->rpc->move($f3->get("CONFIG.account_pool"),$f3->get("CONFIG.account_payout"),doubleval($new_balance),0);
			}
*/
			if ($btcAmount < (float)"0.01") {
				$this->rpc->settxfee((float)$f3->get("CONFIG.transfee"));
			} else {
				$this->rpc->settxfee(0);
			}
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletpassphrase($f3->get("CONFIG.wallet"),2);
			$this->rpc->sendfrom( $f3->get("CONFIG.account_pool"), $from_address , (float)$btcAmount, 0);
			if ($f3->get("CONFIG.wallet")) $this->rpc->walletlock();
			$this->rpc->settxfee(0);
			return true;
		}

	}
?>
