<?
/**
* Bitcoin is a utility class for common functions needed when dealing with bitcoins
*/
	class bitcoin {

		const ADDRESSVERSION="00";

		const BTC = 1;
		const CBTC = 100;
		const MBTC = 1000;
		const UBTC = 1000000;
		const SATOSHI = 100000000;


		static function NumToBase($integer, $base = self::ALLOWED_CHARS) {
		    $length = strlen($base);
		    $out = "";
		    while($integer > $length - 1) {
		    	$mod = floor(bcmod($integer,$length));
		        $out = substr($base,$mod,1) . $out;
		        $integer = floor( $integer / $length );
		    }
		    return substr($base,$integer,1) . $out;
		}

		static function BaseToNum($string, $base = self::ALLOWED_CHARS) {
			$length = strlen($base);
			$size = strlen($string) - 1;
			$string = str_split($string);
			$out = strpos($base, array_pop($string));
			foreach($string as $i => $char) {
				$out += strpos($base, $char) * pow($length, $size - $i);
			}
			return $out;
		}

		static function decodeHex($hex) {
			$hex=strtoupper($hex);
			$chars="0123456789ABCDEF";
			$return="0";
			for($i=0;$i<strlen($hex);$i++) {
				$current=(string)strpos($chars,$hex[$i]);
				$return=(string)bcmul($return,"16",0);
				$return=(string)bcadd($return,$current,0);
			}
			return $return;
		}

		static function encodeHex($dec) {
			$chars="0123456789ABCDEF";
			$return="";
			while (bccomp($dec,0)==1) {
				$dv=(string)bcdiv($dec,"16",0);
				$rem=(integer)bcmod($dec,"16");
				$dec=$dv;
				$return=$return.$chars[$rem];
			}
			return strrev($return);
		}

		static function decodeBase58($base58) {
			$origbase58=$base58;

			$chars="123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
			$return="0";
			for($i=0;$i<strlen($base58);$i++) {
				$current=(string)strpos($chars,$base58[$i]);
				$return=(string)bcmul($return,"58",0);
				$return=(string)bcadd($return,$current,0);
			}

			$return=self::encodeHex($return);

			//leading zeros
			for($i=0;$i<strlen($origbase58)&&$origbase58[$i]=="1";$i++) {
				$return="00".$return;
			}

			if(strlen($return)%2!=0) {
				$return="0".$return;
			}

			return $return;
		}

		static function encodeBase58($hex) {
			if(strlen($hex)%2!=0) {
				die("encodeBase58: uneven number of hex characters");
			}
			$orighex=$hex;

			$chars="123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
			$hex=self::decodeHex($hex);
			$return="";
			while (bccomp($hex,0)==1) {
				$dv=(string)bcdiv($hex,"58",0);
				$rem=(integer)bcmod($hex,"58");
				$hex=$dv;
				$return=$return.$chars[$rem];
			}
			$return=strrev($return);

			//leading zeros
			for($i=0;$i<strlen($orighex)&&substr($orighex,$i,2)=="00";$i+=2) {
				$return="1".$return;
				}

			return $return;
		}

		static function hash160ToAddress($hash160,$addressversion=self::ADDRESSVERSION) {
			$hash160=$addressversion.$hash160;
			$check=pack("H*" , $hash160);
			$check=hash("sha256",hash("sha256",$check,true));
			$check=substr($check,0,8);
			$hash160=strtoupper($hash160.$check);
			return self::encodeBase58($hash160);
		}

		static function addressToHash160($addr) {
			$addr=self::decodeBase58($addr);
			$addr=substr($addr,2,strlen($addr)-10);
			return $addr;
		}

		static function checkAddress($addr,$addressversion=self::ADDRESSVERSION) {
			$addr=self::decodeBase58($addr);
			if(strlen($addr)!=50) {
				return false;
			}
			$version=substr($addr,0,2);
			if(hexdec($version)>hexdec($addressversion)) {
				return false;
			}
			$check=substr($addr,0,strlen($addr)-8);
			$check=pack("H*" , $check);
			$check=strtoupper(hash("sha256",hash("sha256",$check,true)));
			$check=substr($check,0,8);
			return $check==substr($addr,strlen($addr)-8);
		}

		static function hash160($data) {
			$data=pack("H*" , $data);
			return strtoupper(hash("ripemd160",hash("sha256",$data,true)));
		}

		static function pubKeyToAddress($pubkey) {
			return self::hash160ToAddress(self::hash160($pubkey));
		}

		static function remove0x($string) {
			if(substr($string,0,2)=="0x"||substr($string,0,2)=="0X") {
				$string=substr($string,2);
			}
			return $string;
		}

		static function enum($dif) {
			$last_index = 'satoshi';
			$enum_sizes = array(
				'satoshi'=>1,
				'&micro;BTC'=>100,
				'mBTC'=>100000,
				'cBTC'=>1000000,
				//'dBTC'=>10000000,
				'BTC'=>100000000,
				'daBTC'=>1000000000,
				'hBTC'=>10000000000,
				'kBTC'=>100000000000
			);
			foreach($enum_sizes as $name=>$enum) if (bcdiv($dif,$enum) >= 1) $last_index = $name;
			return round( bcdiv($dif, $enum_sizes[$last_index], 8), 2 ). " {$last_index}";
		}

		static function feeSize($inputs=1,$outputs=1) {
			$fee = 0;
			$size = (148 * $inputs) + (34 * $outputs) + 10; //67 inputs with 1 output ~ 10k limit
			if ($size < 10000) {
				$addFee = 0;
			} else {
				$size = bcsub($size,10000);
				$addFee = "0.0005";
				$counts = floor( bcdiv($size,1000) );
				$addFee += bcmul($counts,0.0005);
			}
			$fee = bcadd($fee,$addFee);
			return $fee;
		}

	}
?>