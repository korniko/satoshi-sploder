<div class="row" id="topRow">
	<div class="col-md-3 text-center">
		<IMG class="img-rounded img-polaroid" id="qr" SRC="/qr">
	</div>
	<div class="col-md-6 text-center">
		<IMG SRC="/ui/images/sploder-logo.png" title="SatoshiSploder">
		<p><strong>Bitcoin Quick Raffle Lottery</strong></p>
		<div id="address"><A TARGET="_BLANK" HREF="bitcoin:<?= $address; ?>"><?= $address; ?></A></div>
	</div>
	<div id="buttons" class="col-md-3 text-center">
		<button class="btn btn-large btn-primary" data-target="#help" data-toggle="modal"> How to play </button>
<BR>
		<button class="btn btn-large btn-primary" data-toggle="modal" data-target="#key"> Key </button>
	</div>
</div>
<div class="">
	<div id="odds" class="col-md-3 text-center">
		<table class="table table-striped table-condensed table-bordered">
			<tr valign="top" style="height:30px"><th>#</th><th>Player</th><th>Wager</th><th>Chance</th></tr>
		</table>
	</div>
	<div class="col-md-6 text-center">
		<div id="sploder" class="sploder">
			<div id="stats" class="content text-center"></div>
			<div id="dispTimer" class="content text-center"></div>
		</div>
	</div>
	<div id="history" class="col-md-3 text-center">
		<table class="table table-striped table-condensed table-bordered">
			<tr valign="top" style="height:30px"><td colspan="2"><strong>Winnings History</strong></td></tr>
			<? echo sploder::history(); ?>
		</table>

		<button type="button" class="btn btn-primary" id="mute"><i class="icon-volume-off"></i></button>
	</div>
</div>

<div class="hide" id="chatWrapper">
	<div class="col-md-6 col-md-offset-3 text-center">
		<input class="col-md-5" id="chatInput" type="text" placeholder="Chat">
	</div>
</div>

<div class="modal fade" id="key" tabindex="-1" role="dialog" aria-labelledby="Key" aria-hidden="true">
	<div class='modal-dialog'>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Colors & Values</h3>
			</div>
			<div class="modal-body">
				<h3>Colors</h3>
				<div class="row text-left">
					<div class="col-md-2">
						<IMG SRC="/ui/images/type-you.jpg" class="img-rounded"> You
					</div>
					<div class="col-md-2">
						<IMG SRC="/ui/images/type-others.jpg" class="img-rounded"> Others
					</div>
					<div class="col-md-2">
						<IMG SRC="/ui/images/type-win.jpg" class="img-rounded"> You win
					</div>
				</div>
				<div class="row text-left">
					<div class="col-md-2">
						<IMG SRC="/ui/images/type-lose.jpg" class="img-rounded"> Other win
					</div>
					<div class="col-md-2">
						<IMG SRC="/ui/images/type-empty.jpg" class="img-rounded"> Empty
					</div>
				</div>
		<HR>
		<h3>Values</h3>
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>Name</th><th>Relative</th><th>BTC</th><th>Pronunciation</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1 satoshi</td>
							<td></td>
							<td>0.00000001 BTC</td>
							<td>satoshi</td>
						</tr>
						<tr>
							<td>1 &micro;BTC</td>
							<td>100 satoshi</td>
							<td>0.000001 BTC</td>
							<td>microbit</td>
						</tr>
						<tr>
							<td>1 mBTC</td>
							<td>1,000 &micro;BTC</td>
							<td>0.001 BTC</td>
							<td>milibit</td>
						</tr>
						<tr>
							<td>1 cBTC</td>
							<td>10 mBTC</td>
							<td>0.01 BTC</td>
							<td>bitcent</td>
						</tr>
		<? /*
						<tr class="muted">
							<td>1 dBTC</td>
							<td>10 cBTC</td>
							<td>0.1 BTC</td>
							<td>decibit</td>
						</tr>
		*/ ?>
						<tr>
							<td>1 BTC</td>
							<td>100 cBTC</td>
							<td>1 BTC</td>
							<td>bitcoin</td>
						</tr>
						<tr>
							<td>1 daBTC</td>
							<td>10 BTC</td>
							<td>10 BTC</td>
							<td>decabit</td>
						</tr>
						<tr>
							<td>1 hBTC</td>
							<td>10 daTC</td>
							<td>100 BTC</td>
							<td>hectobit</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-labelledby="Help" aria-hidden="true">
	<div class='modal-dialog'>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>How to play</h3>
			</div>
			<div class="modal-body text-left">
				<ul>
				<li> Contribute to the sploder's pot by transfering bitcoins to your sploder bitcoin address located below the logo or in the QR code.</li>

				<li> At the end of the round the pot will be transfered to a randomly selected player. A player's odds of winning are proportional to their contribution to the pot.</li>

				<li> To initiate a round, a pot must have more than 1 player and have a minimum of 2 cBTC ( 0.02 BTC ).</li>

				<li> If your transaction is recieved after the countdown has ended, then your contribution will continue to the next round.</li>

				<li> A service fee of two percent is deducted from the losing portion of the pot before payout. There is no service fee on the winning portion.</li>

				<li> The bitcoin sploder runs as a service, the website is for visual feedback. You can contribute to the sploder at any time without using the website by sending to the universal sploder address :</li>

				<code>131UzyqpkAUQXLu4owsqGWVgNHNxAkiz1a</code>
				</li>
				</ul>
				<HR>
				<p>Follow us on  Twitter : <A HREF="http://twitter.com/btcsploder" TARGET="_BLANK">@btcsploder</A></p>

			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/ui/js/sploder.js"></script>
<script type="text/javascript" src="/ui/js/front.js"></script>