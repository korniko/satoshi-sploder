var sploder = (function() {
    var d3;
	var path;
	var svg;
	var pie;
	var arc;
	var arcOver;
	var color;
	var width = 460;
	var height = 460;
	var radius = Math.min(width, height) / 2;

    return {
        init: function( d3 ) {
			sploder.d3 = d3;
			sploder.color = sploder.colorMap;
			sploder.pie = sploder.d3.layout.pie()
				.sort(null);

			sploder.arc = sploder.d3.svg.arc()
			    .innerRadius(radius - 100)
			    .outerRadius(radius - 20);

			sploder.arcOver = sploder.d3.svg.arc()
				.innerRadius(radius - 105)
			    .outerRadius(radius - 18);


			sploder.svg = sploder.d3.select("#sploder").append("svg")
			    .attr("width", width)
			    .attr("height", height)
	 			.append("g")
			    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        },
		fillPath : function( index , color ) {
			sploder.d3.selectAll("path")
				.attr("fill", function( d , i ) {
					if (index == (i + 1)) {
						sploder.d3.select(this).transition()
							.duration(1000)
							.attr("d", sploder.arcOver);
							return color;
					}
				});
		},
		updateChart: function( data ) {

			sploder.path = sploder.svg.selectAll("path")
				.data( sploder.pie( data ))
				.enter()
				.append("path")
				.attr("fill", function(d, i) { return sploder.color( i ); })
				.attr("d", function( d ) {
					this._current = d;
					d.startAngle= 3.14159265359 * 2;
					d.endAngle= 3.14159265359 * 2;
					return sploder.arc(d);
				});

			sploder.svg.selectAll("path").transition().duration(2500).attrTween("d", function( a ) {
				var i = sploder.d3.interpolate( this._current, a );
				this._current = i( 0 );
				return function(t) {
					return sploder.arc( i( t ));
				};
			}); // redraw the arcs
		},
		colorMap: function(d) {
			if ((sploder.dataMap[d] == undefined) || (sploder.dataMap[d]['c'] == undefined)) return '#fff';
			return sploder.dataMap[d]['c'];
		},
		reset : function() {
			sploder.dataMap = [];
			sploder.path = sploder.svg.selectAll("path").remove();
		},
		update: function (data) {
			sploder.dataMap = data;
			sploder.remapColors();
			var rdata = sploder.rdata();
			sploder.updateChart( rdata );
		},
		rdata: function() {
			var buildVals=[];
			sploder.dataMap.forEach( function(e,i,a) {
				if ((e['v'] != '') && (e['v'] != undefined)) {
					buildVals.push(e['v']);
				} else {
					buildVals.push(0);
				}
			});
			return buildVals;
		},
		remapColors: function() {
			sploder.d3.selectAll("path").style("fill", function(d, i) { return sploder.color( i ); })
		}
    };
})();
