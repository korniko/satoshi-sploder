var timer;
var disTimer=0;
var disExpire=0;
var soundOn=false;
var snd;

function aChat( j ) {
//Chat
	if (j.p === undefined) j.p = 0;

	if ( (j.p > 0) && ( $("#chatWrapper").css('display') !== 'block') ) {
		$("#chatWrapper").fadeIn();
	}

	if ( (j.p == 0) && ( $("#chatWrapper").css('display') !== 'none') ) {
		$("#chatWrapper").fadeOut();
	}

	if ( j.c !== undefined) {
		if ( j.c.c > 0 ) {
			var dur = Math.round((j.c.m.length / 5) * 10000);

			for (var i in j.c.m) {

				$("#player"+j.c.m[i]['player']).tooltip({
					title: j.c.m[i]['message'],
					placement: 'top',
					delay: {show:0,hide:dur}
				});
				$("#player"+j.c.m[i]['player']).tooltip('show');
			}
		}
	}
}

function aUpdate( j ) {
	sploder.update( j.d );
	tableUpdate( j );
	disExpire = j.e;
	$('#stats').html( j.h );
	if (j.w > 0) {
		if ((soundOn) && (j.w == 1)) snd.play();
		$("#dispTimer").html(' ');
		sploder.fillPath(j.w,j.wc);
		$("#dispTimer").html('Player '+j.w+' wins');
		disTimer = 0;
		timer.stop();
		delete timer;
		timer = $.timer(function() {
			timer.stop();
			delete timer;
			$("#dispTimer").html(' ');
			tableHistory();
			sploder.update( j.d );
			setTimer();
		}, 10000, true); //Reflect Winner for 5 seconds, then resume normal cycle.
	} else {
		if ((j.e > 0) && (disTimer == 0)) { //Timer needs to be set
			disExpire = j.e;
			disTimer = $.timer( updateClock, 1000, true );
		}
	}

	aChat( j );
}

function setTimer() {
	if (timer !== undefined) timer.stop();
	delete timer;
	timer = $.timer(function() {
		$.ajax({
			url: '/',
			dataType: 'json',
			success: aUpdate,
		});
	}, 5000, true);
}

function updateClock() {
	/*
	jQuery(document).ready(function(){
		jQuery("#DIV_TO_PLACE_COUNTDOWN").jCountdown({
			timeText:"2020/01/01 00:00:00",
			timeZone:8,
			style:"flip",
			color:"black",
			width:0,
			textGroupSpace:15,
			textSpace:0,
			reflection:true,
			reflectionOpacity:10,
			reflectionBlur:0,
			dayTextNumber:4,
			displayDay:true,
			displayHour:true,
			displayMinute:true,
			displaySecond:true,
			displayLabel:true,
			onFinish:function(){
				alert("finish");
			}
		});
	});
	*/
	disExpire--;
	if (disExpire < 1) {
		disTimer.stop();
		disTimer=0;
		$("#dispTimer").html(' Choosing ');
	} else {
		$("#dispTimer").html(disExpire);
	}
}

function tableHistory() {
	$("#history table").find("tr:gt(0)").remove();
	$.ajax({
		url: '/history',
		success: function( j ) {
			$('#history tr:last').after( j );
		}
	});
}

function tableUpdate( j ) {
	var count=0;
	$("#odds table").find("tr:gt(0)").remove();
	if (j.t != 0) {
		$(j.d).each( function(i,a) {
			count++;
			if (a['v'] != '') {
				var chance = Math.round( (a['v'] / j.t) * 100 );
				var eclass;
				if (a['i'] != undefined) {
					if (a['i'] == 1) {
						eclass = ' class="success" ';
					} else {
						eclass = ' class="warning" ';
					}
				} else {
					eclass = '';
				}
				$('#odds tr:last').after('<tr '+eclass+'><td>'+count+'</td><td><i class="icon-user icon-2x" id="player'+count+'" style="color:'+a['c']+'"></i></td><td>'+a['vh']+'</td><td>'+chance+'%</td></tr>');
			}
		});
	} else {
		$('#odds tr:last').after('<tr><td colspan="4"><i>Empty</i></td></tr>');
	}
}


/* Exec */

setTimer();
sploder.init( d3 );
$.ajax({
	url: '/',
	dataType: 'json',
	success: function( j ) {
		sploder.update( j.d );
		aUpdate(j);
	}
});

$("#chatInput").keyup( function( event ) {
    if(event.keyCode == 13) {
		var message = $("#chatInput").val();
		$("#chatInput").val("");
		$.ajax({
			url: '/chat',
			data: {"m":message},
			dataType: 'json',
			success: function( j ) {
			}
		});
    }
});

$("#mute").on("click",function() {
	$(this).trigger('button-change');
	if ( ! soundOn ) {
		$(this).html('<i class="icon-volume-up"></i>');
		soundOn = true;
		if (snd === undefined) {
			snd = new Audio("/ui/sounds/ching.mp3");
		}
	} else {
		$(this).html('<i class="icon-volume-off"></i>');
		soundOn = false;
	}
});

