<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <title><?= $META["title"] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="La Korniko">

	<!-- la CDN -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <!-- la stiloj -->
	<link href="/ui/css/font-awesome.min.css" rel="stylesheet" >
	<link href="/ui/jcountdown/jcountdown.css" rel="stylesheet">
	<link href="/ui/css/style.css" rel="stylesheet">

	<!-- Kaj la skriboj -->

	<script src="/ui/js/jquery.numeric.js"></script>
	<script src="/ui/js/jquery.timer.js"></script>
	<script src="/ui/js/jquery.form.js"></script>

	<script src="http://d3js.org/d3.v3.min.js"></script>
	<script src="/ui/jcountdown/jquery.jcountdown.min.js"></script>
	<!-- fek IE -->
	<!--[if IE 7]>
		<link rel="stylesheet" href="/ui/assets/css/font-awesome-ie7.min.css">
	<![endif]-->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->

    <!-- La bildsimboloj -->
    <link rel="shortcut icon" href="/ui/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ui/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ui/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ui/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/ui/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>
  <body>
  <div id="wrapper">
    <div class="container <?=$container_class;?>">
