<BR>
<div class="row">
	<div class="col-md-8 col-md-offset-2 text-center">
		<h1><?= $promoCode; ?></h1>
		<p> Promotional Unit Amount : <?= number_format($promoAmount); ?> Satoshis</p>
		<p> Promotional Units Released : <?= $promoNumber; ?></p>
		<p> Promotional Units Remaining : <?= $promoLeft; ?></p>
	</div>
</div>